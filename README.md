# nuxt2-with-vuetify2

이 문서는 Nuxt 2를 Docker로 돌리기 위한 설명입니다.
전체 디렉토리 구조는 다음과 같습니다.

```
./               # project root directory
  docker/        # Docker에 관련된 directory
    Dockerfile
  frontend/      # Nuxt의 소스코드 directory
    ...          
  .env           #
  compose.yaml   #
```


## Quick Start

작업이전에 [Traefik](https://docs.traefik.io/)이 설치되어 있어야 합니다.

```bash
cp .env.default .env
cp compose.development.yaml compose.yaml 
docker compose build
docker-compose up -d
```
docker가 실행되면 다음 링크를 통해 [nuxt.dev.example](http://nuxt.dev.example) 브라우저에서 확인 가능합니다.


## Bootstrapping

새로운 환경에서 시작하려면 다음 과정을 수행합니다.


### Nuxt Installation

Using [create-nuxt-app](https://github.com/nuxt/create-nuxt-app).
Make sure you have npx installed (npx is shipped by default since npm 5.2.0)

```bash
mkdir <project-name>
cd <project-name>
yarn create nuxt-app <project-name>
```

SSR, Typescript, Axios로 개발한다고 가정하고, 다음 옵션을 선택했습니다.
(선택가능한 옵션과 그 내용에 대해서는 https://github.com/nuxt/create-nuxt-app#features-tada 을 참조)

```
? Project name: <project-name>
? Programming language: TypeScript
? Package manager: Yarn
? UI framework: Vuetify.js
? Nuxt.js modules: Axios - Promise based HTTP client
? Linting tools: None
? Testing framework: None
? Rendering mode: Universal (SSR / SSG)
? Deployment target: Server (Node.js hosting)
? Development tools: jsconfig.json (Recommended for VS Code if you're not using typescript)
? What is your GitHub username? 
? Version control system: Git
```

설치된 Nuxt를 보기 위해 다음 명령을 수행한 후

```bash
cd <project-name>
yarn dev
```

다음 링크를 눌러 http://localhost:3000/ 브라우저로 확인한 후 <kbd>Ctrl</kbd>+<kbd>C</kbd>를 눌러 종료합니다.


### Nuxt Edit host

Docker로 Nuxt에 접근 가능하도록 `<project-name>/nuxt.config.js`에 host 정보를 추가합니다.
```javascript
export default {
  server: {
    host: '0.0.0.0', // default: localhost
  }
  ...
}
```


## Docker compose generation

`compose.yaml`을 생성하여 Nuxt의 기본 Port인 3000번에 대해 접근이 가능하도록 설정하고, 생성한 `./<project-name>`를 volumes에 추가합니다.

```yaml
services:
  frontend:
    image: '<project-name>'
    build:
      context: .
      dockerfile: docker/development/Dockerfile
    restart: always
    labels:
      - 'traefik.enable=true'
      - 'traefik.docker.network=traefik'
      - 'traefik.http.routers.${FRONTEND_TRAEFIK}.rule=Host(`${FRONTEND_DOMAIN}`)'
      - 'traefik.http.routers.${FRONTEND_TRAEFIK}.entrypoints=http'
      - 'traefik.http.services.${FRONTEND_TRAEFIK}.loadbalancer.server.port=3000'
    environment:
      - TZ=${TZ}
      - VUE_APP_FRONTEND_DOMAIN=${FRONTEND_DOMAIN}
    volumes:
      - './<project-name>:/app'
    networks:
      - traefik
networks:
  traefik:
    name: traefik
    external: true
```

## References

* NuxtJS official documentation: https://nuxtjs.org/docs/
* nuxt/create-nuxt-app GitHub repository: https://github.com/nuxt/create-nuxt-app
* NuxtJS documentation - Edit host and port :https://nuxtjs.org/docs/features/configuration/#edit-host-and-port

